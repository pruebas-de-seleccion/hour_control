package es.jorgejbarra.hour_control.workday.infrastructure

import es.jorgejbarra.hour_control.workday.application.GetWorkDayUseCase
import es.jorgejbarra.hour_control.workday.domain.anyEmployeeId
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.mockito.kotlin.mock
import org.springframework.http.HttpStatus.BAD_REQUEST
import java.time.LocalDate

class GetWorkDayEndpointShould {
    private val getWorkDayUseCase = mock<GetWorkDayUseCase>()
    private val getWorkDayEndpoint = GetWorkDayEndpoint(getWorkDayUseCase)

    @Test
    fun `return bad request when end date is before than end date`() {
        // given
        val startDate = LocalDate.now()
        val endDate = startDate.minusDays(1)

        // when
        val response = getWorkDayEndpoint.execute(anyEmployeeId(), startDate, endDate)

        // then
        assertThat(response.statusCode).isEqualTo(BAD_REQUEST)
        assertThat(response.headers["error"]).containsExactly("startDate should be before than endDate")
    }

}