package es.jorgejbarra.hour_control.workday.infrastructure

import es.jorgejbarra.hour_control.share.domain.intervalContaining
import es.jorgejbarra.hour_control.share.randomInstantInADay
import es.jorgejbarra.hour_control.timetrack.domain.TimeTrack
import es.jorgejbarra.hour_control.timetrack.domain.anyTimeTrack
import es.jorgejbarra.hour_control.timetrack.infrastructure.InMemoryTimeTrackRepository
import es.jorgejbarra.hour_control.workday.domain.anyEmployeeId
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.time.LocalDate
import java.time.LocalTime
import java.time.ZoneOffset.UTC
import java.time.temporal.ChronoUnit.DAYS

class InMemoryWorkDayRepositoryShould {

    private val inMemoryTimeTrackRepository = InMemoryTimeTrackRepository()
    private val inMemoryWorkDayRepository = InMemoryWorkDayRepository(inMemoryTimeTrackRepository)

    @Test
    fun `fetch workDay for day and employee`() {
        // given
        val timeTrack = storedTimeTrack()

        // when
        val workDays = inMemoryWorkDayRepository.get(timeTrack.employeeId, intervalContaining(timeTrack))

        // then
        assertThat(workDays).hasSize(1)
        assertThat(workDays[0].timeTracks).hasSize(1)
            .first().apply {
                extracting("time").isEqualTo(LocalTime.ofInstant(timeTrack.date, UTC))
                extracting("serviceId").isEqualTo(timeTrack.serviceId)
                extracting("recordType").isEqualTo(timeTrack.recordType)
                extracting("type").isEqualTo(timeTrack.type)
            }
    }

    @Test
    fun `group tracks by date`() {
        // given
        val timeTrack = storedTimeTrack()

        val anotherTrackInADay = timeTrack.copy(date = randomInstantInADay(timeTrack.date))
        inMemoryTimeTrackRepository.save(anotherTrackInADay)

        // when
        val workDays = inMemoryWorkDayRepository.get(timeTrack.employeeId, intervalContaining(timeTrack, anotherTrackInADay))

        // then
        assertThat(workDays).hasSize(1)
        assertThat(workDays[0].timeTracks).hasSize(2)
    }

    @Test
    fun `fetch only times in interval`() {
        // given
        val employeeId = anyEmployeeId()
        val date = LocalDate.now()

        val timeTrack = anyTimeTrack().copy(employeeId = employeeId, date = randomInstantInADay(date))
        inMemoryTimeTrackRepository.save(timeTrack)

        val timeTrackInOtherDay = anyTimeTrack().copy(
            employeeId = employeeId,
            date = randomInstantInADay(timeTrack.date.plus(1, DAYS))
        )
        inMemoryTimeTrackRepository.save(timeTrackInOtherDay)

        // when
        val workDays = inMemoryWorkDayRepository.get(timeTrack.employeeId, intervalContaining(timeTrack))

        // then
        assertThat(workDays).hasSize(1)
        assertThat(workDays[0].timeTracks).hasSize(1)
    }

    private fun storedTimeTrack(): TimeTrack {
        val timeTrack = anyTimeTrack()
        inMemoryTimeTrackRepository.save(timeTrack)
        return timeTrack
    }
}
