package es.jorgejbarra.hour_control.workday.domain

import es.jorgejbarra.hour_control.share.startTrackTime
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.time.Duration
import java.time.Duration.ZERO
import java.time.LocalTime.now
import java.time.temporal.ChronoUnit.HOURS

class WorkDayShould {
    @Test
    fun `return ordered times from start of the day to end of the day`() {
        // given
        val initialTime = now().plus(2, HOURS)
        val afterInitialTime = initialTime.plus(1, HOURS)

        // when
        val workDay = anyWorkDayWithTimeTracks(
            anyWorkDayTimeTrackInTime(afterInitialTime),
            anyWorkDayTimeTrackInTime(initialTime)
        )

        // then
        assertThat(workDay.timeTracks).extracting("time").containsExactly(initialTime, afterInitialTime)
    }

    @Test
    fun `return 0 duration when do not have work in`() {

        // when
        val anyWorkDayWithTimeTracks = anyWorkDayWithTimeTracks(workEndTimeTrack(now()))

        // then
        assertThat(anyWorkDayWithTimeTracks.duration()).isEqualTo(ZERO)
    }

    @Test
    fun `return work duration when only have one work interval`() {
        // given
        val inTime = startTrackTime()
        val expectedWorkDayDuration = Duration.ofHours(6).plusMinutes(24)

        // when
        val tracks = workEndTimeTrack(inTime.plus(expectedWorkDayDuration))
        val anyWorkDayWithTimeTracks = anyWorkDayWithTimeTracks(
            workStartTimeTrack(inTime),
            tracks
        )

        // then
        assertThat(anyWorkDayWithTimeTracks.duration()).isEqualTo(expectedWorkDayDuration)
    }

    @Test
    fun `sum duration when have more than one work interval`() {
        // given
        val inTime = startTrackTime()
        val durationOfWork = Duration.ofHours(3).plusMinutes(24)

        // when
        val anyWorkDayWithTimeTracks = anyWorkDayWithTimeTracks(
            workStartTimeTrack(inTime),
            workEndTimeTrack(inTime.plus(durationOfWork)),
            workStartTimeTrack(inTime.plus(durationOfWork)),
            workEndTimeTrack(inTime.plus(durationOfWork).plus(durationOfWork))
        )

        // then
        assertThat(anyWorkDayWithTimeTracks.duration()).isEqualTo(durationOfWork.multipliedBy(2))
    }

    @Test
    fun `subtract rest interval from work`() {
        // given
        val inTime = startTrackTime()
        val durationOfWork = Duration.ofHours(3)
        val durationOfRest = Duration.ofMinutes(24)
        val restStart = inTime.plusMinutes(1)

        // when
        val anyWorkDayWithTimeTracks = anyWorkDayWithTimeTracks(
            workStartTimeTrack(inTime),
            restStartTimeTrack(restStart),
            restEndTimeTrack(restStart.plus(durationOfRest)),
            workEndTimeTrack(inTime.plus(durationOfWork).plus(durationOfRest)),
        )

        // then
        assertThat(anyWorkDayWithTimeTracks.duration()).isEqualTo(durationOfWork)
    }
}