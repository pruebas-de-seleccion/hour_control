package es.jorgejbarra.hour_control.workday.domain

import es.jorgejbarra.hour_control.share.anyOf
import es.jorgejbarra.hour_control.share.domain.RecordType
import es.jorgejbarra.hour_control.share.domain.RecordType.IN
import es.jorgejbarra.hour_control.share.domain.RecordType.OUT
import es.jorgejbarra.hour_control.share.domain.Type
import es.jorgejbarra.hour_control.share.domain.Type.REST
import es.jorgejbarra.hour_control.share.domain.Type.WORK
import es.jorgejbarra.hour_control.share.startTrackTime
import java.time.Duration
import java.time.LocalDate
import java.time.LocalTime
import java.util.UUID

fun aNotWarnedWorkDay(): WorkDay {
    val day = LocalDate.now()
    val startTime = startTrackTime()
    return WorkDay(
        employeeId = anyEmployeeId(),
        day = day,
        timeTracks = listOf(
            workStartTimeTrack(startTime),
            workEndTimeTrack(startTime.plus(Duration.ofHours(4)))
        )
    )
}

fun workDayWithDuration(workDayDuration: Duration): WorkDay {
    val inTime = startTrackTime()
    return anyWorkDayWithTimeTracks(
        workStartTimeTrack(inTime),
        workEndTimeTrack(inTime.plus(workDayDuration))
    )
}

fun anyWorkDayWithTimeTracks(vararg tracks: WorkDayTimeTrack): WorkDay {
    return WorkDay(
        employeeId = anyEmployeeId(),
        day = LocalDate.now(),
        timeTracks = tracks.asList()
    )
}

fun anyWorkDayTimeTrackInTime(time: LocalTime): WorkDayTimeTrack = anyOf(workStartTimeTrack(time), workEndTimeTrack(time))

fun workStartTimeTrack(time: LocalTime = startTrackTime()): WorkDayTimeTrack = anyWorkDayTimeTrack().copy(time = time, type = WORK, recordType = IN)
fun workEndTimeTrack(time: LocalTime = startTrackTime()): WorkDayTimeTrack = anyWorkDayTimeTrack().copy(time = time, type = WORK, recordType = OUT)
fun restStartTimeTrack(time: LocalTime = startTrackTime()): WorkDayTimeTrack = anyWorkDayTimeTrack().copy(time = time, type = REST, recordType = IN)
fun restEndTimeTrack(time: LocalTime = startTrackTime()): WorkDayTimeTrack = anyWorkDayTimeTrack().copy(time = time, type = REST, recordType = OUT)

fun anyWorkDayTimeTrack(): WorkDayTimeTrack = WorkDayTimeTrack(
    time = LocalTime.now(),
    recordType = anyOf<RecordType>(),
    type = anyOf<Type>(),
    serviceId = anyServiceId(),
)

fun anyEmployeeId(): String = "Employee-${UUID.randomUUID()}"
fun anyBusinessId(): String = "Business-${UUID.randomUUID()}"
fun anyServiceId(): String = "Service-${UUID.randomUUID()}"
