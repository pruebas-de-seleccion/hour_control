package es.jorgejbarra.hour_control.workday.domain

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.time.Duration.ofHours
import java.time.LocalDate
import java.time.LocalTime
import java.time.temporal.ChronoUnit.HOURS

class WorkDayValidatorShould {

    private val workDayValidator = WorkDayValidator()

    @Test
    fun `no warning when workday is normal`() {
        // given
        val workDayWithoutEndTimeTrack = aNotWarnedWorkDay()

        // when
        val validated = workDayValidator.validate(workDayWithoutEndTimeTrack)

        // then
        assertThat(validated.warnings).isEmpty()
    }

    @Test
    fun `warning when workday don't have end time track`() {
        // given
        val workDayWithoutEndTimeTrack = anyWorkDayWithTimeTracks(workStartTimeTrack())

        // when
        val validated = workDayValidator.validate(workDayWithoutEndTimeTrack)

        // then
        assertThat(validated.warnings).contains(WorkDayWarnings("NO_OUT_TRACK", "Incomplete signings: entry has been signed but not the exit."))
    }


    @Test
    fun `warning when workday don't have in time track`() {
        // given
        val workDayWithoutInTimeTrack = anyWorkDayWithTimeTracks(workEndTimeTrack())

        // when
        val validated = workDayValidator.validate(workDayWithoutInTimeTrack)

        // then
        assertThat(validated.warnings).contains(WorkDayWarnings("NO_IN_TRACK", "Incomplete signings: exit has been signed but not the entry."))
    }

    @Test
    fun `warning when working day is greater than 10 hours `() {  // given
        val workDayDuration = ofHours(10).plusMillis(1)
        val tooLongWorkDay = workDayWithDuration(workDayDuration)

        // when
        val validated = workDayValidator.validate(tooLongWorkDay)

        // then
        assertThat(validated.warnings).contains(WorkDayWarnings("TOO_LONG", "You should work less"))
    }

    @Test
    fun `allow working day of 10 hours `() {  // given
        val validWorkDay = workDayWithDuration(ofHours(10))

        // when
        val validated = workDayValidator.validate(validWorkDay)

        // then
        assertThat(validated.warnings).isEmpty()
    }

    @Test
    fun `start to early anyDay except friday`() {  // given
        val day = LocalDate.parse("2023-04-19") //It's wednesday
        val inTime = LocalTime.of(7, 59)

        val validWorkDay = WorkDay(
            employeeId = anyEmployeeId(),
            day = day,
            timeTracks = listOf(
                workStartTimeTrack(inTime),
                workEndTimeTrack(inTime.plus(3, HOURS))
            )
        )

        // when
        val validated = workDayValidator.validate(validWorkDay)

        // then
        assertThat(validated.warnings).contains(WorkDayWarnings("TOO_EARLY", "The default minimum start hour is 8:00"))
    }

    @Test
    fun `start to early on friday`() {  // given
        val day = LocalDate.parse("2023-04-21") //It's friday
        val inTime = LocalTime.of(6, 59)

        val validWorkDay = WorkDay(
            employeeId = anyEmployeeId(),
            day = day,
            timeTracks = listOf(
                workStartTimeTrack(inTime),
                workEndTimeTrack(inTime.plus(3, HOURS))
            )
        )

        // when
        val validated = workDayValidator.validate(validWorkDay)

        // then
        assertThat(validated.warnings).contains(WorkDayWarnings("TOO_EARLY_ON_FRIDAY", "The minimum start hour for friday is 7:00"))
    }
}