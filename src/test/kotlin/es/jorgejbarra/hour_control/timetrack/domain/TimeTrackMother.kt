package es.jorgejbarra.hour_control.timetrack.domain

import es.jorgejbarra.hour_control.share.anyOf
import es.jorgejbarra.hour_control.share.domain.RecordType
import es.jorgejbarra.hour_control.share.domain.Type
import es.jorgejbarra.hour_control.share.randomInstantInADay
import es.jorgejbarra.hour_control.workday.domain.anyBusinessId
import es.jorgejbarra.hour_control.workday.domain.anyEmployeeId
import es.jorgejbarra.hour_control.workday.domain.anyServiceId
import java.time.Instant
import java.time.LocalDate


fun anyTimeTrackForEmployee(employeeId: String): TimeTrack = anyTimeTrack()
    .copy(employeeId = employeeId)

fun anyTimeTrackInDay(date: LocalDate): TimeTrack = anyTimeTrack().copy(date = randomInstantInADay(date))

fun anyTimeTrack(): TimeTrack = TimeTrack(
    employeeId = anyEmployeeId(),
    businessId = anyBusinessId(),
    serviceId = anyServiceId(),
    date = Instant.now(),
    recordType = anyOf<RecordType>(),
    type = anyOf<Type>()
)
