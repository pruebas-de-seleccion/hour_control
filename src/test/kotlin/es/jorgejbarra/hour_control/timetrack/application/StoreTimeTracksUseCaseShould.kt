package es.jorgejbarra.hour_control.timetrack.application

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.kotlinModule
import es.jorgejbarra.hour_control.timetrack.domain.anyTimeTrack
import es.jorgejbarra.hour_control.timetrack.infrastructure.TimeTrackRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

class StoreTimeTracksUseCaseShould {
    private val objectMapper: ObjectMapper = ObjectMapper().registerModules(JavaTimeModule(), kotlinModule())

    private val timeTrackRepository = mock<TimeTrackRepository>()
    private val storeTimeTracksUseCase = StoreTimeTracksUseCase(timeTrackRepository)

    @Test
    fun `return error request when file is unreadable`() {
        // given
        val unreadableFileContent = "content".toByteArray()

        // when
        val result = storeTimeTracksUseCase.execute(unreadableFileContent)

        // then
        assertThat(result.isFailure).isTrue
    }

    @Test
    fun `store tracks`() {
        // given
        val expectedTrackToSave = anyTimeTrack()
        val readableFileContent = objectMapper.writeValueAsBytes(listOf(expectedTrackToSave))

        // when
        val result = storeTimeTracksUseCase.execute(readableFileContent)

        // then
        assertThat(result.isFailure).isFalse
        verify(timeTrackRepository).save(expectedTrackToSave)
    }
}