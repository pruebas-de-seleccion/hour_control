package es.jorgejbarra.hour_control.timetrack.infrastructure

import es.jorgejbarra.hour_control.timetrack.application.StoreTimeTracksUseCase
import es.jorgejbarra.hour_control.timetrack.domain.UnreadableFileException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import org.springframework.http.HttpStatus.BAD_REQUEST
import org.springframework.http.MediaType.TEXT_PLAIN_VALUE
import org.springframework.mock.web.MockMultipartFile
import org.springframework.web.multipart.MultipartFile
import kotlin.Result.Companion.failure


class StoreTimeTracksEndpointShould {

    private val storeTimeTracksUseCase = mock<StoreTimeTracksUseCase>()
    private val storeTimeTracksEndpoint: StoreTimeTracksEndpoint = StoreTimeTracksEndpoint(storeTimeTracksUseCase)

    @Test
    fun `return bad request when file is unreadable`() {
        // given
        val result: MultipartFile = MockMultipartFile(
            "testing-filename.json", "testing-filename.json",
            TEXT_PLAIN_VALUE, "content".toByteArray()
        )

        whenever(storeTimeTracksUseCase.execute(any())).thenReturn(failure(UnreadableFileException()))

        // when
        val response = storeTimeTracksEndpoint.execute(result)

        // then
        assertThat(response.statusCode).isEqualTo(BAD_REQUEST)
        assertThat(response.headers["error"]).containsExactly("unreadable file")
    }
}