package es.jorgejbarra.hour_control.timetrack.infrastructure

import es.jorgejbarra.hour_control.share.domain.intervalAfter
import es.jorgejbarra.hour_control.share.domain.intervalBefore
import es.jorgejbarra.hour_control.share.domain.intervalContaining
import es.jorgejbarra.hour_control.timetrack.domain.TimeTrack
import es.jorgejbarra.hour_control.timetrack.domain.anyTimeTrack
import es.jorgejbarra.hour_control.timetrack.domain.anyTimeTrackForEmployee
import es.jorgejbarra.hour_control.workday.domain.anyEmployeeId
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test


class InMemoryTimeTrackRepositoryShould {

    private val inMemoryTimeTrackRepository = InMemoryTimeTrackRepository()

    @Test
    fun `store time track`() {
        // given
        val newTimeTrack = anyTimeTrack()

        // when
        inMemoryTimeTrackRepository.save(newTimeTrack)

        // then
        assertThat(inMemoryTimeTrackRepository.find(newTimeTrack.employeeId, intervalContaining(newTimeTrack)))
            .containsExactly(newTimeTrack)
    }

    @Test
    fun `adds new elements to stored previously`() {
        // given
        val previouslySaved = storedTimeTrack()

        val newTimeTrack = anyTimeTrackForEmployee(previouslySaved.employeeId)

        // when
        inMemoryTimeTrackRepository.save(newTimeTrack)

        // then
        val foundTracksForEmployee = inMemoryTimeTrackRepository.find(previouslySaved.employeeId, intervalContaining(previouslySaved, newTimeTrack))
        assertThat(foundTracksForEmployee).containsExactlyInAnyOrder(newTimeTrack, previouslySaved)
    }

    @Test
    fun `filter when end date is before`() {
        // given
        val newTimeTrack = storedTimeTrack()


        // when
        val foundTracks = inMemoryTimeTrackRepository.find(newTimeTrack.employeeId, intervalBefore(newTimeTrack))

        // then
        assertThat(foundTracks).isEmpty()
    }

    @Test
    fun `filter when from date is after`() {
        // given
        val newTimeTrack = storedTimeTrack()

        // when
        val foundTracks = inMemoryTimeTrackRepository.find(newTimeTrack.employeeId, intervalAfter(newTimeTrack))

        // then
        assertThat(foundTracks).isEmpty()
    }

    private fun storedTimeTrack(): TimeTrack {
        val previouslySaved = anyTimeTrackForEmployee(anyEmployeeId())
        inMemoryTimeTrackRepository.save(previouslySaved)
        return previouslySaved
    }
}
