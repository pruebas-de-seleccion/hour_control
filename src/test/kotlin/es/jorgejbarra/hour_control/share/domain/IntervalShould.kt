package es.jorgejbarra.hour_control.share.domain

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.time.Instant
import java.time.LocalDate
import java.time.ZoneOffset.UTC

class IntervalShould {
    @Test
    fun `contains from date`() {
        // given
        val fromDate = Instant.now()

        // when
        val interval = Interval.from(fromDate, Instant.now().plusSeconds(1))

        // then
        assertThat(interval.contains(fromDate)).isTrue
    }

    @Test
    fun `not contains end date`() {
        // given
        val endDate = Instant.now()

        // when
        val interval = Interval.from(endDate.minusSeconds(1), endDate)

        // then
        assertThat(interval.contains(endDate)).isFalse
    }

    @Test
    fun `contains a date inside the interval`() {
        // given
        val containedInstant = Instant.now()
        val from = containedInstant.minusSeconds(1)
        val to = containedInstant.plusSeconds(1)

        // when
        val interval = Interval.from(from, to)

        // then
        assertThat(interval.contains(containedInstant)).isTrue
    }

    @Test
    fun `not contains a date before the interval`() {
        // given
        val instantBefore = Instant.now()
        val from = instantBefore.plusSeconds(1)

        // when
        val interval = Interval.from(from, from.plusSeconds(1))

        // then
        assertThat(interval.contains(instantBefore)).isFalse
    }

    @Test
    fun `not contains a date after the interval`() {
        // given
        val instantAfter = Instant.now()
        val to = instantAfter.minusSeconds(1)

        // when
        val interval = Interval.from(to.minusSeconds(1), to)

        // then
        assertThat(interval.contains(instantAfter)).isFalse
    }

    @Test
    fun `contains start of the day when create from date`() {
        // given
        val aDay = LocalDate.now()

        // when
        val interval = Interval.from(aDay, aDay)

        // then
        assertThat(interval.contains(aDay.atStartOfDay().toInstant(UTC))).isTrue
    }

    @Test
    fun `not contains start of the next day when create from date`() {
        // given
        val aDay = LocalDate.now()
        val aNexDay = LocalDate.now().plusDays(1)

        // when
        val interval = Interval.from(aDay, aDay)

        // then
        assertThat(interval.contains(aNexDay.atStartOfDay().toInstant(UTC))).isFalse
    }
}