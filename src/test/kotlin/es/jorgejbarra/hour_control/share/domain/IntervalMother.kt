package es.jorgejbarra.hour_control.share.domain

import es.jorgejbarra.hour_control.timetrack.domain.TimeTrack

fun intervalContaining(track: TimeTrack): Interval {
    return Interval.from(track.date.minusMillis(1), track.date.plusMillis(1))
}

fun intervalContaining(vararg tracks: TimeTrack): Interval {
    val orderedTracks = tracks.asList().sortedBy { timeTrack -> timeTrack.date }
    return Interval.from(orderedTracks.first().date.minusMillis(1), orderedTracks.last().date.plusMillis(1))
}

fun intervalBefore(track: TimeTrack): Interval {
    val to = track.date.minusMillis(1)
    return Interval.from(to.minusMillis(1), to)
}

fun intervalAfter(track: TimeTrack): Interval {
    val from = track.date.minusMillis(1)
    return Interval.from(from, from.plusMillis(1))
}