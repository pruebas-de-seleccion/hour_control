package es.jorgejbarra.hour_control.share.domain

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class ListSplitterShould {

    @Test
    fun `do not found interval whe never close a interval`() {
        // given
        val openInterval = { x: Int -> x == 1 }
        val neverClosePredicate = { _: Int -> false }

        // when
        val foundIntervals = listOf(1, 2, 3).splitInIntervalsIgnoringFirst(openInterval, neverClosePredicate)

        // then
        Assertions.assertThat(foundIntervals).isEmpty()
    }

    @Test
    fun `do not found interval when never open a interval`() {
        // given
        val closeInterval = { x: Int -> x == 3 }
        val neverOpenPredicate = { _: Int -> false }

        // when
        val foundIntervals = listOf(1, 2, 3).splitInIntervalsIgnoringFirst(neverOpenPredicate, closeInterval)

        // then
        Assertions.assertThat(foundIntervals).isEmpty()
    }

    @Test
    fun `ignore invalid intervals at start`() {
        // given
        val openPredicate = { x: Int -> x == 1 }
        val closePredicate = { x: Int -> x == 3 }

        // when
        val foundIntervals = listOf(1, 2, 1, 2, 3).splitInIntervalsIgnoringFirst(openPredicate, closePredicate)

        // then
        Assertions.assertThat(foundIntervals).hasSize(1).first().isEqualTo(listOf(1, 2, 3))
    }

    @Test
    fun `ignore all until found open`() {
        // given
        val openPredicate = { x: Int -> x == 1 }
        val closePredicate = { x: Int -> x == 3 }

        // when
        val foundIntervals = listOf(2, 2, 2, 1, 2, 3).splitInIntervalsIgnoringFirst(openPredicate, closePredicate)

        // then
        Assertions.assertThat(foundIntervals).hasSize(1).first().isEqualTo(listOf(1, 2, 3))
    }

    @Test
    fun `ignore invalid intervals at the end`() {
        // given
        val openPredicate = { x: Int -> x == 1 }
        val closePredicate = { x: Int -> x == 3 }

        // when
        val foundIntervals = listOf(1, 2, 3, 1, 2).splitInIntervalsIgnoringFirst(openPredicate, closePredicate)

        // then
        Assertions.assertThat(foundIntervals).hasSize(1).first().isEqualTo(listOf(1, 2, 3))
    }

    @Test
    fun `found multiple  intervals `() {
        // given
        val openPredicate = { x: Int -> x == 1 }
        val closePredicate = { x: Int -> x == 3 }

        // when
        val foundIntervals = listOf(1, 2, 3, 1, 3).splitInIntervalsIgnoringFirst(openPredicate, closePredicate)

        // then
        Assertions.assertThat(foundIntervals).hasSize(2).containsExactly(
            listOf(1, 2, 3),
            listOf(1, 3)
        )
    }

    @Test
    fun `ignore elements between intervals`() {
        // given
        val openPredicate = { x: Int -> x == 1 }
        val closePredicate = { x: Int -> x == 3 }

        // when
        val foundIntervals = listOf(1, 2, 3, 4, 5, 1, 3).splitInIntervalsIgnoringFirst(openPredicate, closePredicate)

        // then
        Assertions.assertThat(foundIntervals).hasSize(2).containsExactly(
            listOf(1, 2, 3),
            listOf(1, 3)
        )
    }
}