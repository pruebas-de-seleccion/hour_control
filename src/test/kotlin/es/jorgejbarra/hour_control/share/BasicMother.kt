package es.jorgejbarra.hour_control.share

import java.util.function.Supplier
import kotlin.random.Random


private const val MAX_ELEMENT_IN_DEFAULT_LIST = 5

fun <T> anyOf(examples: Collection<T>): T = examples.shuffled().first()
fun <T> anyOf(vararg examples: T): T = examples.toList().shuffled().first()
inline fun <reified T : Enum<T>> anyOf(): T {
    return enumValues<T>().toList().shuffled().first()
}

/**
 * Return list with aleatory length between 0 and maxElement
 */
fun <T> anyListOf(maxElement: Int = MAX_ELEMENT_IN_DEFAULT_LIST, generator: Supplier<T>): List<T> =
    anyList(0, maxElement, generator)

/**
 * Return list with aleatory length between 1 and maxElement
 */
fun <T> anyListNotEmptyOf(maxElement: Int = MAX_ELEMENT_IN_DEFAULT_LIST, generator: Supplier<T>): List<T> =
    anyList(1, maxElement, generator)

/**
 * Return list with aleatory length between minElement and maxElement
 */
fun <T> anyList(minElement: Int, maxElement: Int, generator: Supplier<T>): List<T> =
    buildList { repeat(Random.nextInt(minElement, maxElement)) { add(generator.get()) } }

fun <T> anyOrNull(generator: Supplier<T>): T? = Random.nextBoolean()
    .takeIf { it }
    ?.let { generator.get() }
