package es.jorgejbarra.hour_control.share

import java.time.Instant
import java.time.LocalDate
import java.time.LocalTime
import java.time.ZoneId.systemDefault
import java.time.ZoneOffset.UTC
import kotlin.random.Random

private const val millisInDay = 24 * 60 * 60 * 1000L
fun randomInstantInADay(date: LocalDate): Instant {
    return date.atStartOfDay(UTC).toInstant()
        .plusMillis(Random.nextLong(millisInDay))
}

fun startTrackTime(): LocalTime {
    return LocalTime.of(10, 0)
}

fun randomInstantInADay(dateTime: Instant): Instant {
    return LocalDate.ofInstant(dateTime, systemDefault())
        .atStartOfDay(UTC).toInstant()
        .plusMillis(Random.nextLong(millisInDay))
}