package es.jorgejbarra.hour_control

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class HourControlApplication

fun main(args: Array<String>) {
	runApplication<HourControlApplication>(*args)
}
