package es.jorgejbarra.hour_control.workday.infrastructure

import es.jorgejbarra.hour_control.share.domain.Interval
import es.jorgejbarra.hour_control.workday.application.GetWorkDayUseCase
import es.jorgejbarra.hour_control.workday.domain.ValidatedWorkDay
import es.jorgejbarra.hour_control.workday.domain.WorkDayTimeTrack
import es.jorgejbarra.hour_control.workday.domain.WorkDayWarnings
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDate
import java.time.format.DateTimeFormatter.ISO_LOCAL_DATE
import java.time.format.DateTimeFormatter.ISO_LOCAL_TIME
import kotlin.Result.Companion.failure
import kotlin.Result.Companion.success

@RestController
class GetWorkDayEndpoint(
    private val getWorkDayUseCase: GetWorkDayUseCase,
) {
    @GetMapping("/employees/{employeeId}/workday")
    fun execute(
        @PathVariable("employeeId") employeeId: String,
        @RequestParam("startDate", required = true) startDate: LocalDate,
        @RequestParam("endDate", required = true) endDate: LocalDate,
    ): ResponseEntity<List<ValidatedWorkDayResponse>> {
        return intervalFrom(startDate, endDate)
            .fold(
                onSuccess = { interval -> getWorkDayUseCase.execute(employeeId, interval).map(this::toResponse).let { ResponseEntity.ok(it) } },
                onFailure = { badIntervalResponse() }
            )
    }

    private fun intervalFrom(startDate: LocalDate, endDate: LocalDate): Result<Interval> = when {
        startDate.isBefore(endDate) -> success(Interval.from(startDate, endDate))
        else -> failure(IllegalArgumentException("startDate should be before than endDate"))
    }

    private fun toResponse(source: ValidatedWorkDay): ValidatedWorkDayResponse = ValidatedWorkDayResponse(
        employeeId = source.workDay.employeeId,
        day = ISO_LOCAL_DATE.format(source.workDay.day),
        duration = source.workDay.duration().toString(),
        tracks = source.workDay.timeTracks.map(this::toResponse),
        warnings = source.warnings.map(this::toResponse)
    )

    private fun toResponse(source: WorkDayWarnings): WorkDayWarningsResponse = WorkDayWarningsResponse(
        code = source.code,
        description = source.description
    )

    private fun toResponse(source: WorkDayTimeTrack): WorkDayTimeTrackResponse = WorkDayTimeTrackResponse(
        time = ISO_LOCAL_TIME.format(source.time),
        recordType = source.recordType.name,
        type = source.type.name,
    )

    private fun badIntervalResponse(): ResponseEntity<List<ValidatedWorkDayResponse>> =
        ResponseEntity.badRequest()
            .header("error", "startDate should be before than endDate")
            .build()
}
