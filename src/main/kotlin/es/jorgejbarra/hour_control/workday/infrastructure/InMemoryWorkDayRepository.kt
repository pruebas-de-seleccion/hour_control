package es.jorgejbarra.hour_control.workday.infrastructure

import es.jorgejbarra.hour_control.share.domain.Interval
import es.jorgejbarra.hour_control.timetrack.domain.TimeTrack
import es.jorgejbarra.hour_control.timetrack.infrastructure.TimeTrackRepository
import es.jorgejbarra.hour_control.workday.domain.WorkDay
import es.jorgejbarra.hour_control.workday.domain.WorkDayRepository
import es.jorgejbarra.hour_control.workday.domain.WorkDayTimeTrack
import org.springframework.stereotype.Repository
import java.time.Instant
import java.time.LocalDate
import java.time.LocalTime
import java.time.ZoneOffset.UTC


@Repository
class InMemoryWorkDayRepository(
    private val timeTrackRepository: TimeTrackRepository
) : WorkDayRepository {
    override fun get(employeeId: String, interval: Interval): List<WorkDay> {
        return timeTrackRepository.find(employeeId, interval)
            .groupByDay()
            .map {
                WorkDay(
                    employeeId = employeeId,
                    day = it.key,
                    it.value.map { track ->
                        WorkDayTimeTrack(
                            serviceId = track.serviceId,
                            time = track.date.time,
                            recordType = track.recordType,
                            type = track.type
                        )
                    }
                )
            }
    }

    private fun Collection<TimeTrack>.groupByDay(): Map<LocalDate, List<TimeTrack>> = groupBy { LocalDate.ofInstant(it.date, UTC) }

    private val Instant.time: LocalTime
        get() = LocalTime.ofInstant(this, UTC)

}
