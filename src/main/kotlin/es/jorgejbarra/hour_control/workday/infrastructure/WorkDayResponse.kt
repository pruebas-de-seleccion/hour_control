package es.jorgejbarra.hour_control.workday.infrastructure

data class ValidatedWorkDayResponse(
    val employeeId: String,
    val day: String,
    val duration: String,
    val tracks: List<WorkDayTimeTrackResponse>,
    val warnings: List<WorkDayWarningsResponse>
)

data class WorkDayWarningsResponse(val code: String, val description: String)

data class WorkDayTimeTrackResponse(
    val time: String,
    val recordType: String,
    val type: String,
)
