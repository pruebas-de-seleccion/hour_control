package es.jorgejbarra.hour_control.workday.application

import es.jorgejbarra.hour_control.share.domain.Interval
import es.jorgejbarra.hour_control.workday.domain.ValidatedWorkDay
import es.jorgejbarra.hour_control.workday.domain.WorkDayRepository
import es.jorgejbarra.hour_control.workday.domain.WorkDayValidator
import org.springframework.stereotype.Service

@Service
class GetWorkDayUseCase(
    private val workDayRepository: WorkDayRepository,
    private val workDayValidator: WorkDayValidator
) {
    fun execute(employeeId: String, interval: Interval): List<ValidatedWorkDay> = workDayRepository.get(employeeId, interval)
        .map(workDayValidator::validate)
}