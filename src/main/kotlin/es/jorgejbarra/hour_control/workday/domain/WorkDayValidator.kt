package es.jorgejbarra.hour_control.workday.domain

import es.jorgejbarra.hour_control.workday.domain.checker.FridayStartTimeChecker
import es.jorgejbarra.hour_control.workday.domain.checker.IncompleteWorkDayChecker
import es.jorgejbarra.hour_control.workday.domain.checker.MondayToThursdayStartTimeChecker
import es.jorgejbarra.hour_control.workday.domain.checker.TooLongWorkDayChecker
import es.jorgejbarra.hour_control.workday.domain.checker.WorkDayChecker
import org.springframework.stereotype.Service

@Service
class WorkDayValidator {
    private val checkers: Collection<WorkDayChecker> = listOf(
        IncompleteWorkDayChecker(),
        TooLongWorkDayChecker(),
        FridayStartTimeChecker(),
        MondayToThursdayStartTimeChecker()
    )

    fun validate(source: WorkDay): ValidatedWorkDay = ValidatedWorkDay(workDay = source, warnings = collectWarnings(source))

    private fun collectWarnings(source: WorkDay) = checkers.mapNotNull { it.check(source) }

}
