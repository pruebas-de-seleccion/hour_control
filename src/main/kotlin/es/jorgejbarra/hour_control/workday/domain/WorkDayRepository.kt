package es.jorgejbarra.hour_control.workday.domain

import es.jorgejbarra.hour_control.share.domain.Interval

interface WorkDayRepository {
    fun get(employeeId: String, interval: Interval): List<WorkDay>
}