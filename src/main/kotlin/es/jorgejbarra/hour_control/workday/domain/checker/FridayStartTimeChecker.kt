package es.jorgejbarra.hour_control.workday.domain.checker

import es.jorgejbarra.hour_control.workday.domain.WorkDay
import es.jorgejbarra.hour_control.workday.domain.WorkDayWarnings
import java.time.DayOfWeek.FRIDAY
import java.time.LocalTime

class FridayStartTimeChecker : WorkDayChecker {

    private var minTrackTime = LocalTime.of(7, 0, 0)
    override fun check(source: WorkDay): WorkDayWarnings? {
        if (source.getDayOfWeek() != FRIDAY) {
            return null
        }

        if (source.getStartDateTime().isBefore(minTrackTime)) {
            return WorkDayWarnings.tooEarlyOnFriday()
        }

        return null
    }

}
