package es.jorgejbarra.hour_control.workday.domain

data class ValidatedWorkDay(
    val workDay: WorkDay,
    val warnings: List<WorkDayWarnings>
)

data class WorkDayWarnings(
    val code: String,
    val description: String
) {
    companion object {
        fun noOutTrack(): WorkDayWarnings = WorkDayWarnings(code = "NO_OUT_TRACK", description = "Incomplete signings: entry has been signed but not the exit.")
        fun noInTrack(): WorkDayWarnings = WorkDayWarnings(code = "NO_IN_TRACK", description = "Incomplete signings: exit has been signed but not the entry.")
        fun tooLong(): WorkDayWarnings = WorkDayWarnings(code = "TOO_LONG", description = "You should work less")
        fun tooEarlyOnFriday(): WorkDayWarnings = WorkDayWarnings(code = "TOO_EARLY_ON_FRIDAY", description = "The minimum start hour for friday is 7:00")
        fun tooEarly(): WorkDayWarnings = WorkDayWarnings(code = "TOO_EARLY", description = "The default minimum start hour is 8:00")
    }
}
