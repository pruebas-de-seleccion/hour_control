package es.jorgejbarra.hour_control.workday.domain

import es.jorgejbarra.hour_control.share.domain.splitInIntervalsIgnoringFirst
import java.time.DayOfWeek
import java.time.Duration
import java.time.Duration.ZERO
import java.time.LocalDate
import java.time.LocalTime

class WorkDay(
    val employeeId: String,
    val day: LocalDate,
    timeTracks: List<WorkDayTimeTrack>
) {
    val timeTracks: List<WorkDayTimeTrack> = timeTracks.sortedBy(WorkDayTimeTrack::time)

    fun duration(): Duration {
        val workTimeWithRest = findWorkInterval()
            .map { Duration.between(it.first().time, it.last().time) }
            .fold(ZERO, Duration::plus)

        val restDuration = findRestInterval()
            .map { Duration.between(it.first().time, it.last().time) }
            .fold(ZERO, Duration::plus)

        return workTimeWithRest.minus(restDuration)
    }

    private fun findWorkInterval() = timeTracks
        .splitInIntervalsIgnoringFirst(
            openIntervalPredicate = { timeTrack -> timeTrack.isWorkIn },
            closeIntervalPredicate = { timeTrack -> timeTrack.isWorkOut }
        )

    private fun findRestInterval() = timeTracks
        .splitInIntervalsIgnoringFirst(
            openIntervalPredicate = { timeTrack -> timeTrack.isRestIn },
            closeIntervalPredicate = { timeTrack -> timeTrack.isRestOut }
        )

    fun getDayOfWeek(): DayOfWeek = day.dayOfWeek
    fun getStartDateTime(): LocalTime = timeTracks.first().time

}
