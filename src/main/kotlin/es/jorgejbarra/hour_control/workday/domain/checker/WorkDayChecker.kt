package es.jorgejbarra.hour_control.workday.domain.checker

import es.jorgejbarra.hour_control.workday.domain.WorkDay
import es.jorgejbarra.hour_control.workday.domain.WorkDayWarnings

interface WorkDayChecker {
    fun check(source: WorkDay): WorkDayWarnings?
}