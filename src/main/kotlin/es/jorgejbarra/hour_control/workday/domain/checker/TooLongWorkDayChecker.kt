package es.jorgejbarra.hour_control.workday.domain.checker

import es.jorgejbarra.hour_control.workday.domain.WorkDay
import es.jorgejbarra.hour_control.workday.domain.WorkDayWarnings
import java.time.Duration

class TooLongWorkDayChecker : WorkDayChecker {
    private val maxWorkDuration = Duration.ofHours(10)

    override fun check(source: WorkDay): WorkDayWarnings? {
        if (source.duration() > maxWorkDuration) {
            return WorkDayWarnings.tooLong()
        }

        return null
    }

}

