package es.jorgejbarra.hour_control.workday.domain

import es.jorgejbarra.hour_control.share.domain.RecordType
import es.jorgejbarra.hour_control.share.domain.RecordType.IN
import es.jorgejbarra.hour_control.share.domain.RecordType.OUT
import es.jorgejbarra.hour_control.share.domain.Type
import es.jorgejbarra.hour_control.share.domain.Type.REST
import es.jorgejbarra.hour_control.share.domain.Type.WORK
import java.time.LocalTime

data class WorkDayTimeTrack(
    val serviceId: String,
    val time: LocalTime,
    val recordType: RecordType,
    val type: Type,
) {
    val isWorkIn: Boolean
        get() = recordType == IN && type == WORK
    val isWorkOut: Boolean
        get() = recordType == OUT && type == WORK
    val isRestIn: Boolean
        get() = recordType == IN && type == REST
    val isRestOut: Boolean
        get() = recordType == OUT && type == REST
    val isIn: Boolean
        get() = recordType == IN
    val isOut: Boolean
        get() = recordType == OUT
}