package es.jorgejbarra.hour_control.workday.domain.checker

import es.jorgejbarra.hour_control.workday.domain.WorkDay
import es.jorgejbarra.hour_control.workday.domain.WorkDayWarnings

class IncompleteWorkDayChecker : WorkDayChecker {
    override fun check(source: WorkDay): WorkDayWarnings? {

        val numberOfOut = source.timeTracks.filter { timeTrack -> timeTrack.isOut }.size
        val numberOfIn = source.timeTracks.filter { timeTrack -> timeTrack.isIn }.size

        if (numberOfOut > numberOfIn) {
            return WorkDayWarnings.noInTrack()
        }

        if (numberOfOut < numberOfIn) {
            return WorkDayWarnings.noOutTrack()
        }

        return null
    }

}

