package es.jorgejbarra.hour_control.share.domain

enum class Type {
    WORK, REST
}