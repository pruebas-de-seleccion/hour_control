package es.jorgejbarra.hour_control.share.domain

import java.time.Instant
import java.time.LocalDate
import java.time.ZoneOffset

class Interval private constructor(
    private val fromDate: Instant,
    private val toDate: Instant
) {
    fun contains(time: Instant): Boolean {
        return !time.isBefore(fromDate)
                && !time.isAfter(toDate)
                && time != toDate
    }

    companion object {
        fun from(from: Instant, to: Instant): Interval {
            return Interval(from, to)
        }

        fun from(from: LocalDate, to: LocalDate): Interval {
            return Interval(atStartOfDay(from), atStartOfNextDay(to))
        }

        private fun atStartOfDay(source: LocalDate): Instant {
            return source.atStartOfDay().toInstant(ZoneOffset.UTC)
        }

        private fun atStartOfNextDay(source: LocalDate): Instant {
            return source.plusDays(1).atStartOfDay().toInstant(ZoneOffset.UTC)
        }
    }

}