package es.jorgejbarra.hour_control.share.domain

enum class RecordType {
    IN, OUT
}