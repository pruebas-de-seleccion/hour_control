package es.jorgejbarra.hour_control.share.domain


fun <T> List<T>.splitInIntervalsIgnoringFirst(
    openIntervalPredicate: (T) -> Boolean,
    closeIntervalPredicate: (T) -> Boolean
): List<List<T>> {
    return jumpElementsToFirstOpen(openIntervalPredicate)
        .splitForPredicate(openIntervalPredicate)
        .closeIntervalsOrEmpty(closeIntervalPredicate)
        .filter(List<T>::isNotEmpty)
}

private fun <T> List<T>.jumpElementsToFirstOpen(openIntervalPredicate: (T) -> Boolean): List<T> {

    val indexOfFirst = indexOfFirst(openIntervalPredicate)
    if (indexOfFirst == -1) {
        return emptyList()
    }

    return subList(indexOfFirst, size)
}

private fun <T> List<List<T>>.closeIntervalsOrEmpty(closeIntervalPredicate: (T) -> Boolean): List<List<T>> {
    return map {
        val indexOfFirst = it.indexOfFirst(closeIntervalPredicate)
        if (indexOfFirst == -1) {
            emptyList()
        } else {
            it.subList(0, indexOfFirst + 1)
        }
    }
}

private fun <T> List<T>.splitForPredicate(openIntervalPredicate: (T) -> Boolean) =
    flatMapIndexed { index, x ->
        when {
            index == lastIndex -> listOf(index)
            openIntervalPredicate(x) && index == 0 -> listOf(0)
            openIntervalPredicate(x) -> listOf(index - 1, index)
            else -> emptyList()
        }
    }
        .windowed(size = 2, step = 2) { (from, to) -> this.slice(from..to) }