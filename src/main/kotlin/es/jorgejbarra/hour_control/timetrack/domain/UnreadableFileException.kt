package es.jorgejbarra.hour_control.timetrack.domain

class UnreadableFileException : Throwable()
