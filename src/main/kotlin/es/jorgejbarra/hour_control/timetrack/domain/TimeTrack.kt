package es.jorgejbarra.hour_control.timetrack.domain

import es.jorgejbarra.hour_control.share.domain.RecordType
import es.jorgejbarra.hour_control.share.domain.Type
import java.time.Instant

data class TimeTrack(
    val employeeId: String,
    val businessId: String,
    val serviceId: String,
    val date: Instant,
    val recordType: RecordType,
    val type: Type,
)

