package es.jorgejbarra.hour_control.timetrack.application

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.kotlinModule
import es.jorgejbarra.hour_control.timetrack.domain.TimeTrack
import es.jorgejbarra.hour_control.timetrack.infrastructure.TimeTrackRepository
import org.springframework.stereotype.Service

@Service
class StoreTimeTracksUseCase(
    private val timeTrackRepository: TimeTrackRepository
) {
    private val objectMapper: ObjectMapper = ObjectMapper().registerModules(JavaTimeModule(), kotlinModule())
    fun execute(fileContent: ByteArray): Result<List<TimeTrack>> {
        return runCatching { objectMapper.readValue(fileContent, Array<TimeTrack>::class.java).asList() }
            .onSuccess { it.onEach(timeTrackRepository::save) }
    }
}
