package es.jorgejbarra.hour_control.timetrack.infrastructure

import es.jorgejbarra.hour_control.share.domain.Interval
import es.jorgejbarra.hour_control.timetrack.domain.TimeTrack
import org.springframework.stereotype.Repository
import java.util.LinkedList

@Repository
class InMemoryTimeTrackRepository : TimeTrackRepository {
    private final val storage: MutableList<TimeTrack> = LinkedList()
    override fun save(elementToAdd: TimeTrack) {
        storage.add(elementToAdd)
    }

    override fun find(employeeId: String, includedIn: Interval): List<TimeTrack> {
        return storage.filter { timeTrack -> timeTrack.employeeId == employeeId }
            .filter { timeTrack -> includedIn.contains(timeTrack.date) }
    }
}
