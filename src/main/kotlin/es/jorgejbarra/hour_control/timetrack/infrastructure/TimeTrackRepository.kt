package es.jorgejbarra.hour_control.timetrack.infrastructure

import es.jorgejbarra.hour_control.share.domain.Interval
import es.jorgejbarra.hour_control.timetrack.domain.TimeTrack

interface TimeTrackRepository {
    fun save(elementToAdd: TimeTrack)
    fun find(employeeId: String, includedIn: Interval): List<TimeTrack>
}