package es.jorgejbarra.hour_control.timetrack.infrastructure

import es.jorgejbarra.hour_control.timetrack.application.StoreTimeTracksUseCase
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.badRequest
import org.springframework.http.ResponseEntity.ok
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.multipart.MultipartFile


@Controller
class StoreTimeTracksEndpoint(
    private val storeTimeTracksUsecase: StoreTimeTracksUseCase,
) {
    @PostMapping("/timeTracks")
    fun execute(
        @RequestParam("file") file: MultipartFile
    ): ResponseEntity<Void> {
        return storeTimeTracksUsecase.execute(file.bytes)
            .fold(
                onSuccess = { ok().build() },
                onFailure = { unreadableFileErrorResponse() }
            )
    }

    private fun unreadableFileErrorResponse(): ResponseEntity<Void> = badRequest()
        .header("error", "unreadable file")
        .build()
}