package es.jorgejbarra.hour_control.test

import es.jorgejbarra.hour_control.clients.HourControlClient
import es.jorgejbarra.hour_control.timetrack.domain.anyTimeTrack
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT
import org.springframework.http.HttpStatus.OK
import java.time.Instant
import java.time.LocalDate
import java.time.ZoneOffset.UTC


@SpringBootTest(webEnvironment = DEFINED_PORT)
class StoreTimeTrackEndpointShould {

    private val hourControlClient = HourControlClient()

    @Test
    fun `store time track`() {
        // given
        val aTimeTrack = anyTimeTrack()

        // when
        val storeResponse = hourControlClient.storeTimeTrack(aTimeTrack)

        // then
        assertThat(storeResponse.statusCode).isEqualTo(OK)

        val timeTrackDay = getDay(aTimeTrack.date)
        val response = hourControlClient.fetchEmployeeWorkDay(aTimeTrack.employeeId, timeTrackDay)
        assertThat(response.statusCode).isEqualTo(OK)
        assertThat(response.body).anySatisfy { workDay -> WorkDayAssertion.assertThat(workDay).contains(aTimeTrack) }
    }

    private fun getDay(date: Instant): LocalDate = LocalDate.ofInstant(date, UTC)
}
