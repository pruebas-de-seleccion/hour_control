package es.jorgejbarra.hour_control.test

import es.jorgejbarra.hour_control.timetrack.domain.TimeTrack
import es.jorgejbarra.hour_control.workday.infrastructure.ValidatedWorkDayResponse
import org.assertj.core.api.AbstractAssert
import org.assertj.core.api.Assertions.assertThat
import java.time.LocalTime
import java.time.ZoneOffset.UTC

class WorkDayAssertion(actual: ValidatedWorkDayResponse?) : AbstractAssert<WorkDayAssertion, ValidatedWorkDayResponse>(actual, WorkDayAssertion::class.java) {

    fun contains(expectedTimeTrack: TimeTrack) {
        assertThat(actual).isNotNull

        assertThat(actual.employeeId)
            .describedAs("not same employeeId")
            .isEqualTo(expectedTimeTrack.employeeId)

        assertThat(actual.tracks)
            .describedAs("not contains expected time track '$expectedTimeTrack'")
            .anyMatch { track ->
                LocalTime.parse(track.time).equals(LocalTime.ofInstant(expectedTimeTrack.date, UTC))
                        && track.recordType == expectedTimeTrack.recordType.name
                        && track.type == expectedTimeTrack.type.name
            }
    }

    companion object {
        fun assertThat(actual: ValidatedWorkDayResponse?): WorkDayAssertion {
            return WorkDayAssertion(actual)
        }
    }
}