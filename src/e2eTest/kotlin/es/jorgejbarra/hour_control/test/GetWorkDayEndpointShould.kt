package es.jorgejbarra.hour_control.test

import es.jorgejbarra.hour_control.actions.HourControlActions
import es.jorgejbarra.hour_control.clients.HourControlClient
import es.jorgejbarra.hour_control.test.WorkDayAssertion.Companion.assertThat
import es.jorgejbarra.hour_control.workday.domain.anyEmployeeId
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT
import org.springframework.http.HttpStatus.OK
import java.time.LocalDate


@SpringBootTest(webEnvironment = DEFINED_PORT)
class GetWorkDayEndpointShould {
    private val hourControlClient = HourControlClient()
    private val hourControlActions = HourControlActions()

    @Test
    fun `return tracks for given employee`() {
        // given
        val employeeId = anyEmployeeId()
        val aDay = LocalDate.now()
        val storedTimeTrackInADay = hourControlActions.givenStoredTimeTrackInADayForEmployeeId(employeeId, aDay)

        // when
        val response = hourControlClient.fetchEmployeeWorkDay(employeeId, aDay)

        // then
        assertThat(response.statusCode).isEqualTo(OK)

        assertThat(response.body).hasSize(1)
        response.body!!.first().also { workDay -> storedTimeTrackInADay.forEach(assertThat(workDay)::contains) }
    }

    @Test
    fun `not return other days tracks`() {
        // given
        val employeeId = anyEmployeeId()
        val aDay = LocalDate.now()
        hourControlActions.givenStoredTimeTrackInADayForEmployeeId(employeeId, aDay)

        // when
        val response = hourControlClient.fetchEmployeeWorkDay(employeeId, aDay.plusDays(1))

        // then
        assertThat(response.statusCode).isEqualTo(OK)
        assertThat(response.body).isEmpty()
    }
}
