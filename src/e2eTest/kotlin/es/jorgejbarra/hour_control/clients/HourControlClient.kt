package es.jorgejbarra.hour_control.clients

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jsonMapper
import com.fasterxml.jackson.module.kotlin.kotlinModule
import es.jorgejbarra.hour_control.timetrack.domain.TimeTrack
import es.jorgejbarra.hour_control.workday.infrastructure.ValidatedWorkDayResponse
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.ContentDisposition
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType.MULTIPART_FORM_DATA
import org.springframework.http.ResponseEntity
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.client.RestTemplate
import java.time.LocalDate
import java.time.format.DateTimeFormatter.ISO_LOCAL_DATE

class HourControlClient {
    private val jsonMapper = jsonMapper { addModules(kotlinModule(), JavaTimeModule()) }

    private val httpClient = RestTemplate()

    fun fetchEmployeeWorkDay(
        employeeId: String,
        aDay: LocalDate,
    ): ResponseEntity<List<ValidatedWorkDayResponse>> {
        return httpClient.exchange("http://localhost:8080/employees/$employeeId/workday?startDate=${ISO_LOCAL_DATE.format(aDay)}&endDate=${ISO_LOCAL_DATE.format(aDay.plusDays(1))}",
            HttpMethod.GET,
            null,
            object : ParameterizedTypeReference<List<ValidatedWorkDayResponse>>() {})
    }

    fun storeTimeTrack(vararg tracks: TimeTrack): ResponseEntity<Void> {
        return storeTimeTrack(tracks.asList())
    }

    fun storeTimeTrack(tracks: Collection<TimeTrack>): ResponseEntity<Void> {

        val body: MultiValueMap<String, Any> = LinkedMultiValueMap<String, Any>().apply {
            val headers = LinkedMultiValueMap<String, String>().apply {
                val contentDisposition = ContentDisposition
                    .builder("form-data")
                    .name("file")
                    .filename("virtual-file.json")
                    .build()
                add(HttpHeaders.CONTENT_DISPOSITION, contentDisposition.toString())
            }

            add("file", HttpEntity(jsonMapper.writeValueAsBytes(tracks), headers))
        }

        return httpClient.exchange(
            "http://localhost:8080/timeTracks",
            HttpMethod.POST,
            HttpEntity(body, HttpHeaders().apply { contentType = MULTIPART_FORM_DATA }),
            Void::class.java
        )
    }

}