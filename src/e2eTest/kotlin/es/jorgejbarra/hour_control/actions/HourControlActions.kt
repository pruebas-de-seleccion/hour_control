package es.jorgejbarra.hour_control.actions

import es.jorgejbarra.hour_control.clients.HourControlClient
import es.jorgejbarra.hour_control.share.anyListNotEmptyOf
import es.jorgejbarra.hour_control.timetrack.domain.TimeTrack
import es.jorgejbarra.hour_control.timetrack.domain.anyTimeTrackInDay
import es.jorgejbarra.hour_control.workday.domain.anyBusinessId
import java.time.LocalDate

class HourControlActions {

    private val hourControlClient = HourControlClient()

    fun givenStoredTimeTrackInADayForEmployeeId(employeeId: String, aDate: LocalDate): List<TimeTrack> {
        val businessId = anyBusinessId()

        val tracks = anyListNotEmptyOf {
            anyTimeTrackInDay(aDate)
                .copy(employeeId = employeeId)
                .copy(businessId = businessId)
        }
        hourControlClient.storeTimeTrack(tracks)
        return tracks
    }
}