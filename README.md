Control horario!!!
========================

# Welcome!

Hola gente!!!

Esta es la solución propuesta para la prueba. Está escrita en kotlin porque me proporciona mucha velocidad de desarrollo y mucha azúcar sintáctica para implementar buenas practicas. Espero que os
venga bien :)

# Descripción del problema

Me salto esta parte, si tenéis alguna duda podéis consultar vuestro pdf en [INSTRUCTIONS.pdf](INSTRUCTIONS.pdf)

# Que necesitas para empezar a ver esta prueba

- Java 17 (no uso la nada especial de java 17 pero ¯\_(ツ)_/¯)
- Internet!!!!! porque gradle necesita descargar dependencias
- No necesitas gradle, yo subo el wrapper por ti :P
- Respira y ten en cuenta que yo también soy humano y suelo equivocarme :)

### Construyendo el proyecto

Este es un proyecto spring boot construido con gradle, como siempre puedes construirlo desde consola:

```shell
./gradlew build
```

### Ejecutar el proyecto!!!

Este es un proyecto spring boot construido con gradle, como siempre puedes iniciar la consola del servidor con:

```shell
./gradlew clean build bootRun
```

# ¿Que he tenido en consideración?

- **Usé Kotlin**: Al principio me sentí tentado a elegir java, pero luego pensé que podría demostrar mi experiencia en java dado mi CV. Por eso me cambio a kotlin, que me ayuda mucho a desarrollar
  rápidamente (esto
  es importante porque quiero disfrutar de mi tiempo libre). **No he hecho ninguna anotación sobre el lenguaje kotlin. Siempre podemos quedar y lo vemos juntos.**
- **Usé DDD**: Puedes comprobar que no empecé usando DDD, después hice una pequeña estructuración para sacar capas y añadir las piezas que me faltaban. Además aunque por el tamaño de la prueba no
  tiene mucho sentido he dividido el dominio en dos.
- **Usé TDD**: No siempre, pero puedes ver en el histórico de commit como he ido creciendo explorando las soluciones con los test en verde.
- **He eliminado el dia de los picajes de un dia de trabajo**: Despues de realizar todo el trabajo arrastrando la fecha por los picajes, he detectado que en el dominio no tenia mucho sentido mantener
  esta información duplicada porque en un día de trabajo solo había horas de es día. Finalmente he decidido simplificar el dominio y eso ha generado una mayor simplicidad en la generación de ejemplos
  de test y en las operaciones.
- **No he sobre-pensado**: He decidido no sobre-pensar y dejar muchas soluciones algo vagas, la razón es que valoro mi tiempo y hay algunos problemas muy chulos aquí que requieren demasiado.
  - Se ha decidido no normalizar los datos, por ejemplo con autocierres
  - La duración de los días de trabajo tiene mucho que mejorar, por ejemplo los horarios de descanso fuera de horario de trabajo se imputan como descanso.
  - No he realizado una serialización customizada de la duración de un workday.
  - He eliminado de la lectura los datos superfluos como bussinesId, porque no se usan para nada.
  - No he realizado una importación parcial validada. El archivo se importa de forma total o no se importa. En el ejemplo había picajes sin hora.
- **No he guardado los warnings**: He preferido calcularlos al vuelo.
- **He segundo el patrón ObjectMother:** Adoro este patrón!!!! Simplifica mucho el crecimiento de los test y el mantenimiento a futuro.

## ¿Como he probado?

He creado 2 tipos de test: Unitarios y Aceptación. Como no he creado repositorios reales hacia ningún sistema de storage no he necesitado test de integración.

- **Aceptación**: He creado algunos test estimulando el sistema desde fuera. He creado también la infraestructura necesaria para que crear nuevos test de este tipo sea modular, haciendo que el equipo
  pueda ampliar esta suit rápidamente a medida que amplia la funcionalidad
- **Unitarios**: Una unidad lógica -> un test.
  - Como ejemplo y para facilitar el debate, he dejado como una única unidad lógica testeable el validado/checker de warnings de workday. Sin embargo está compuesto para facilitar su ampliación por
    múltiples checkers. El equipo podría decidir dividir esta unidad lógica de testing y hacer test para cada uno de sus componentes por separado

# What could be improved?

- Todo por defecto

## Mandatory Gif

![mandatory gif](https://media.giphy.com/media/Mliueouehmpag/giphy.gif)

**Esto es todo amigos.**
